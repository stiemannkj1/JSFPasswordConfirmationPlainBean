This is the sample code for my blog entitled "Password Confirmation on a JSF Page � Part 1 A Simple Model" that you will find at https://www.omnijava.com. Its default setup is to throw an exception when the Save button is pressed. See the blog to learn why and how to fix the problem.

The application server can be either GlassFish or Payara. I use Payara.

The project was created with NetBeans 8.2 but it uses the Maven build system so it can be opened in Eclipse or IntelliJ.
