package com.jsfpasswordconfirmplainbean.backingbean;

import com.jsfpasswordconfirmplainbean.model.User;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Backing bean for index.xhtml
 *
 * @author Ken Fogel
 */
@Named("passwordBacking")
@RequestScoped
public class PasswordBackingBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(PasswordBackingBean.class);

    @Inject
    private User user;

    // The value for the confirmPassword field that does not exist in the entity
    private String passwordConfirm;

    /**
     * Return the User model that has been injected into this backing bean
     *
     * @return The User object
     */
    public User getUser() {
        LOG.trace("getUser");
        return user;
    }

    /**
     * In this example pressing the save button will just lead to another page
     * if validation was successful.
     *
     * @return A String that indicates the next page to go to
     */
    public String doSomething() {
        LOG.trace("doSomething");
        return "doSomething";
    }

    /**
     * Getter for the password confirm field
     *
     * @return
     */
    public String getPasswordConfirm() {
        LOG.trace("getPasswordConfirm");
        return passwordConfirm;
    }

    /**
     * Setter for the password confirm field
     *
     * @param passwordConfirm
     */
    public void setPasswordConfirm(String passwordConfirm) {
        LOG.trace("setPasswordConfirm");
        this.passwordConfirm = passwordConfirm;
    }

    /**
     * The method that compares the two password fields incorrectly
     *
     * @param context
     * @param component
     * @param value
     */
    public void validatePasswordError(FacesContext context, UIComponent component,
            Object value) {

        LOG.debug("validatePasswordError: " + user.getPassword() + " and " + passwordConfirm);
        if (!user.getPassword().equals(passwordConfirm)) {
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['nomatch']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
            throw new ValidatorException(msg);
        }
    }

    /**
     * The method that compares the two password fields correctly
     *
     * @param context
     * @param component
     * @param value
     */
    public void validatePasswordCorrect(FacesContext context, UIComponent component,
            Object value) {

        LOG.trace("validatePasswordCorrect");
        
        // Retrieve the value passed to this method
        String confirmPassword = (String) value;

        // Retrieve the temporary value from the password field
        UIInput passwordInput = (UIInput) component.findComponent("password");
        String password = (String) passwordInput.getLocalValue();

        LOG.debug("validatePasswordCorrect01: " + password + " and " + confirmPassword);
        if (password == null || confirmPassword == null || !password.equals(confirmPassword)) {
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['nomatch']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
            throw new ValidatorException(msg);
        }
    }
}
